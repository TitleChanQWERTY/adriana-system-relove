from random import randint

import pygame
from group_conf import item_group
from particle import AreaParticle
from scene_config import particle_sprite
from config_script import sc

sprite = {"exp1": pygame.image.load("ASSETS/sprite/item/exp/1.png").convert_alpha(),
          "exp2": pygame.image.load("ASSETS/sprite/item/exp/2.png").convert_alpha()}


class Exp(pygame.sprite.Sprite):
    def __init__(self, x, y, type_exp):
        super().__init__()
        self.sprite = (sprite["exp1"], sprite["exp2"])
        self.typeExp = type_exp
        self.image = self.sprite[self.typeExp]
        self.image = pygame.transform.scale(self.image, (33, 33))

        self.rect = self.image.get_rect(center=(x, y+1))

        self.add(item_group)

        self.idItem: int = type_exp

        self.itmeLive: int = 0
        self.itemLiveMax: int = randint(120, 190)

        self.speedX: int = randint(-1, 1)
        self.speedY: int = randint(-1, 1)

        if self.speedY == 0:
            self.speedY = 1
        if self.speedX == 0:
            self.speedX = -1

        self.timeCreateParticle: int = 0

        self.x = x
        self.y = y

        self.Angle = 0
        self.AngleSet = randint(-1, 1)
        if self.AngleSet == 0:
            self.AngleSet = 1

    def update(self):
        pygame.draw.circle(sc, (22, 243, 245), self.rect.center, 25, 1)
        if self.timeCreateParticle >= 20:
            AreaParticle(self.rect.centerx, self.rect.centery, particle_sprite["2"], (randint(6, 10), randint(6, 10)),
                         randint(90, 185))
            self.timeCreateParticle = 0
        self.timeCreateParticle += 1
        if self.rect.y < -55 or self.rect.y > 745 or self.rect.x < -45 or self.rect.x > 1295:
            self.kill()

        self.x += self.speedX
        self.y += self.speedY

        self.image = self.sprite[self.typeExp]
        self.image = pygame.transform.scale(self.image, (33, 33))

        self.Angle += self.AngleSet
        self.image = pygame.transform.rotate(self.image, self.Angle)

        self.rect = self.image.get_rect(center=(self.x, self.y))
