from random import randint

import pygame

from config_script import sc
from group_conf import item_group
from particle import AreaParticle

sprite = {"1": pygame.image.load("ASSETS/sprite/item/ammo.png").convert_alpha()}
particle_sprite = {"1": pygame.image.load("ASSETS/sprite/particle/8.png").convert_alpha()}


class Ammo(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.timeCreateParticle = 0
        self.AngleSet = randint(-3, 3)
        self.Angle = 0
        if self.AngleSet == 0:
            self.AngleSet = 1
        self.image = sprite["1"]
        self.image = pygame.transform.scale(self.image, (31, 31))
        self.rect = self.image.get_rect(center=(x, y))

        self.sprite = sprite["1"]

        self.add(item_group)

        self.idItem: int = 2

        self.speedX: int = randint(-2, 2)
        self.speedY: int = randint(-2, 2)

        if self.speedY == 0:
            self.speedY = 1

        self.x = x
        self.y = y

    def update(self):
        pygame.draw.circle(sc, (250, 250, 0), self.rect.center, 25, 2)
        if self.timeCreateParticle >= 15:
            size = randint(6, 11)
            AreaParticle(self.rect.centerx, self.rect.centery, particle_sprite["1"], (size, size),
                         randint(90, 185))
            self.timeCreateParticle = 0
        self.timeCreateParticle += 1
        if self.rect.y < -35 or self.rect.y > 735 or self.rect.x < -35 or self.rect.y > 1280:
            self.kill()

        self.x += self.speedX
        self.y += self.speedY

        self.image = self.sprite
        self.image = pygame.transform.scale(self.image, (31, 31))

        self.Angle += self.AngleSet
        self.image = pygame.transform.rotate(self.image, self.Angle)

        self.rect = self.image.get_rect(center=(self.x, self.y))
