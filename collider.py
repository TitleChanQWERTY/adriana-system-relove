from random import randint

import pygame.image

import group_conf
from scene import scene_game
import scene_config
from particle import PhysiclParticle, AreaParticle
from fill_screen import Fill
from txtMoveCreate import Txt

particle_sprite = {"1": pygame.image.load("ASSETS/sprite/particle/blood/1.png").convert_alpha(),
                   "2": pygame.image.load("ASSETS/sprite/particle/blood/2.png").convert_alpha(),
                   "3": pygame.image.load("ASSETS/sprite/particle/7.png").convert_alpha()}

trash_particle = {"1": pygame.image.load("ASSETS/sprite/particle/trash/1.png").convert_alpha(),
                  "2": pygame.image.load("ASSETS/sprite/particle/trash/2.png").convert_alpha(),
                  "3": pygame.image.load("ASSETS/sprite/particle/trash/3.png").convert_alpha(),
                  "4": pygame.image.load("ASSETS/sprite/particle/trash/4.png").convert_alpha()}


def colliderItem():
    for item in group_conf.item_group:
        if scene_config.PLAYER.rect.colliderect(item.rect):
            if item.idItem == 0 or item.idItem == 1:
                EXP_SET = 5
                if item.idItem == 1:
                    EXP_SET += 5
                scene_config.PLAYER.EXP += EXP_SET
                Txt(item.rect.centerx, item.rect.centery, "+" + str(EXP_SET), (22, 243, 245), 23)
                scene_config.PLAYER.SCORE += 268
                scene_config.PLAYER.set_exp()
                item.kill()
            if item.idItem == 2:
                Txt(item.rect.centerx, item.rect.centery, "+150", (255, 255, 0), 23)
                scene_config.PLAYER.AMMO += 150
                scene_config.PLAYER.SCORE += 75
                item.kill()


def colliderBullet():
    if scene_config.PLAYER.timeDamage >= scene_config.PLAYER.maxTimeDamage:
        scene_config.PLAYER.image.set_alpha(300)
        for bullet_enemy in group_conf.enemy_bullet:
            if scene_config.PLAYER.rect.collidepoint(bullet_enemy.rect.center):
                scene_config.PLAYER.timeDamage = 0
                for i in range(4):
                    scene_config.PLAYER.ForceMove[i] = 0
                scene_config.PLAYER.HEALTH -= bullet_enemy.damage
                scene_config.PLAYER.rect.y += bullet_enemy.damage + 10
                Fill(290, -17, 1, (255, 0, 0))
                scene_game.screen_shake = 24
                scene_game.screenShake_power = 10
                bullet_enemy.kill()
    else:
        scene_config.PLAYER.timeDamage += 1
        scene_config.PLAYER.image.set_alpha(100)

    for player_bullet in group_conf.player_bullet:
        for enemy in group_conf.enemy_group:
            if enemy.rect.y > -25:
                if enemy.rect.collidepoint(player_bullet.rect.center):
                    enemy.rect.y -= randint(0, 3)
                    enemy.rect.x -= randint(-1, 1)
                    enemy.FrameDamage = 0
                    enemy.Health -= scene_config.PLAYER.DAMAGE
                    sprite = (particle_sprite["1"], particle_sprite["2"])
                    if enemy.idEnemy == 0:
                        for i in range(2):
                            PhysiclParticle(enemy.rect.centerx, enemy.rect.centery, randint(5, 14), randint(-2, 2),
                                            sprite[randint(0, len(sprite) - 1)], (randint(8, 10), randint(8, 10)))
                    else:
                        for i in range(3):
                            AreaParticle(enemy.rect.centerx, enemy.rect.centery, particle_sprite["3"], (7, 7),
                                         randint(165, 250))
                    player_bullet.kill()

    for player_bullet in group_conf.player_bomb:
        for e_b in group_conf.enemy_bullet:
            if e_b.rect.colliderect(player_bullet.rect):
                e_b.kill()
        for enemy in group_conf.enemy_group:
            if enemy.rect.colliderect(player_bullet.rect):
                enemy.FrameDamage = 0
                enemy.Health -= scene_config.PLAYER.DAMAGE+5
                sprite = (particle_sprite["1"], particle_sprite["2"])
                if enemy.idEnemy == 0:
                    for i in range(2):
                        PhysiclParticle(enemy.rect.centerx, enemy.rect.centery, randint(5, 14), randint(-2, 2),
                                        sprite[randint(0, len(sprite) - 1)], (randint(8, 10), randint(8, 10)))
                else:
                    for i in range(3):
                        AreaParticle(enemy.rect.centerx, enemy.rect.centery, particle_sprite["3"], (7, 7),
                                     randint(165, 250))
