from random import randint

import pygame
from group_conf import player_bomb
from scene import scene_game

bomb_sprite = {"1": pygame.image.load("ASSETS/sprite/bullet/player/bomb.png").convert_alpha()}

explosion_bomb = {"1": pygame.image.load("ASSETS/sprite/effect/bomb/1.png").convert_alpha(),
                  "2": pygame.image.load("ASSETS/sprite/effect/bomb/2.png").convert_alpha()}


class BombArea(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.sprite = (explosion_bomb["1"], explosion_bomb["2"])
        self.selectSprite = self.sprite[randint(0, len(self.sprite) - 1)]
        self.image = self.selectSprite
        self.image = pygame.transform.scale(self.image, (1, 1))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(player_bomb)

        self.x = x
        self.y = y

        self.size: int = 1

        self.timeLive: int = 0

        self.alpha = 1265

    def update(self):
        if self.size < 260:
            self.size += 39

        self.alpha -= 17

        self.timeLive += 1

        if self.alpha <= 0:
            self.kill()

        self.image = self.selectSprite
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.image.set_alpha(self.alpha)
        self.rect = self.image.get_rect(center=(self.x, self.y))


class Bomb(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = bomb_sprite["1"]
        self.image = pygame.transform.scale(self.image, (17, 17))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(player_bomb)

        self.speed: int = -12

        self.timeBoom: int = 0

        self.x = x
        self.y = y

        self.Angle: int = 0

    def update(self):
        if self.timeBoom >= 45 and self.rect.y > 116:
            scene_game.screen_shake = 35
            scene_game.screenShake_power = 28
            BombArea(self.rect.centerx, self.rect.centery)
            self.kill()
        self.timeBoom += 1

        if self.rect.y <= 2:
            self.speed = randint(18, 26)
        if self.rect.y >= 695:
            self.speed = randint(-16, -11)

        self.y += self.speed

        self.image = bomb_sprite["1"]
        self.image = pygame.transform.scale(self.image, (17, 17))

        self.Angle += 15
        self.image = pygame.transform.rotate(self.image, self.Angle)

        self.rect = self.image.get_rect(center=(self.x, self.y))
