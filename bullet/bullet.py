import pygame


class SimpleBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size, speed, group, damage=6, flip=False):
        super().__init__()
        self.image = filename
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(group)

        self.damage = damage

        self.speed: int = speed

        self.flip = flip

        if self.flip:
            self.image = pygame.transform.flip(self.image, False, True)

    def update(self):
        if self.rect.y < 15 or self.rect.y > 735:
            self.kill()

        self.rect.y += self.speed


class PersonalMoveBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size, speed_x, speed_y, group, damage=15):
        super().__init__()
        self.image = filename
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.speedX = speed_x
        self.speedY = speed_y

        self.add(group)

        self.damage = damage

    def update(self):
        if self.rect.y < 14 or self.rect.y > 735:
            self.kill()

        self.rect.y += self.speedY
        self.rect.x += self.speedX
