from random import randint

import pygame

from collider import trash_particle
from scene_config import particle_sprite
from group_conf import enemy_group, enemy_bullet
from bullet import bullet
from item import exp
from particle import AreaParticle
from scene_config import PLAYER
from txtMoveCreate import Txt
from scene import scene_game

sprite_ship = {"1": pygame.image.load("ASSETS/sprite/enemy/ship/humiliator/1.png").convert_alpha(),
               "2": pygame.image.load("ASSETS/sprite/enemy/ship/striker/1.png").convert_alpha()}

sprite_trash = (trash_particle["1"], trash_particle["2"], trash_particle["3"], trash_particle["4"])


class Humiliator(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = sprite_ship["1"]
        self.image = pygame.transform.scale(self.image, (69, 72))
        self.rect = self.image.get_rect(center=(-900, -900))

        self.sprite_bullet = {"1": pygame.image.load("ASSETS/sprite/bullet/enemy/2.png").convert_alpha()}

        self.image = pygame.transform.flip(self.image, False, True)

        self.Health: int = 265

        self.idEnemy: int = 1

        self.add(enemy_group)

        self.timeStartMove: int = 0

        self.maxTimeStartMove: int = randint(1240, 3050)

        self.CountShoot: int = 0
        self.timeBullet: int = 0

        self.FrameDamage: int = 4

        self.size = 62

        self.speed: int = randint(1, 2)

        self.dX, self.dY = randint(-2, 2), 1

        if self.dX == 0:
            self.dX = 1

        self.timeLive: int = 0

    def update(self):
        if self.timeStartMove <= self.maxTimeStartMove * 4:
            if self.timeStartMove == self.maxTimeStartMove * 4:
                self.rect = self.image.get_rect(center=(randint(157, 857), -25))
            self.timeStartMove += 1
        else:
            if self.Health <= 0:
                scene_game.screen_shake = 66
                scene_game.screenShake_power = 23
                for i in range(randint(15, 20)):
                    AreaParticle(self.rect.centerx, self.rect.centery, particle_sprite["1"], (12, 3))
                for b in range(randint(20, 35)):
                    size = randint(12, 37)
                    AreaParticle(self.rect.centerx, self.rect.centery, sprite_trash[randint(0, len(sprite_trash) - 1)],
                                 (size, size))
                PLAYER.SCORE += 355
                exp.Exp(self.rect.centerx, self.rect.centery, 1)
                Txt(self.rect.centerx, self.rect.centery, "+355")
                self.rect = self.image.get_rect(center=(-1000, -1000))
                self.timeStartMove = 0
                self.maxTimeStartMove: int = randint(1355, 4155)
                self.timeLive = 0
                self.Health = 265

            if self.CountShoot < 100:
                bullet.SimpleBullet(self.rect.centerx + 1, self.rect.centery + 5, self.sprite_bullet["1"],
                                    (6, 26), 19, enemy_bullet)
                self.CountShoot += 1
            else:
                if self.timeBullet > 225:
                    self.timeBullet = 0
                    self.CountShoot = 0
                self.timeBullet += 1

            if self.rect.x < 65 or self.rect.x > 1200 - self.size:
                self.dX = -self.dX

            if self.rect.y < 25 and self.timeLive > 95 or self.rect.y > 575 - self.size:
                self.dY = -self.dY

            if self.timeLive <= 95:
                self.timeLive += 1
            else:
                self.rect.y = max(24, min(574, self.rect.y))

            self.rect.x += self.speed * self.dX
            self.rect.y += self.speed * self.dY

            self.image = sprite_ship["1"]

            if self.FrameDamage < 9:
                self.image = pygame.image.load("ASSETS/sprite/enemy/ship/humiliator/damage.png").convert_alpha()
                self.FrameDamage += 1
            self.image = pygame.transform.scale(self.image, (69, 72))
            self.image = pygame.transform.flip(self.image, False, True)


class Striker(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = sprite_ship["2"]
        self.image = pygame.transform.scale(self.image, (37, 41))
        self.rect = self.image.get_rect(center=(-900, -900))

        self.bullet_sprite = {"1": pygame.image.load("ASSETS/sprite/bullet/enemy/3.png").convert_alpha()}

        self.add(enemy_group)

        self.idEnemy: int = 1

        self.Health: int = 155

        self.timeStartMove: int = 0

        self.maxTimeStartMove: int = randint(650, 4555)

        self.FrameDamage: int = 4

        self.typeStriker: int = randint(0, 2)

        self.speed: int = 1
        if self.typeStriker == 0:
            self.speed = -1

        self.timeShoot = 0

    def update(self):
        if self.timeStartMove <= self.maxTimeStartMove * 4:
            if self.timeStartMove == self.maxTimeStartMove:
                if self.typeStriker >= 1:
                    self.rect = self.image.get_rect(center=(randint(47, 1150), -35))
                else:
                    self.rect = self.image.get_rect(center=(randint(47, 1150), 755))
            self.timeStartMove += 1
        else:
            self.rect.y += self.speed
            if self.rect.y < -65 or self.rect.y > 765 or self.Health <= 0:
                if self.Health <= 0:
                    scene_game.screen_shake = 17
                    scene_game.screenShake_power = randint(2, 3)
                    for i in range(randint(7, 17)):
                        size = randint(11, 14)
                        AreaParticle(self.rect.centerx, self.rect.centery,
                                     sprite_trash[randint(0, len(sprite_trash) - 1)],
                                     (size, size))
                        AreaParticle(self.rect.centerx, self.rect.centery, particle_sprite["1"], (12, 3))
                    PLAYER.SCORE += 205
                    Txt(self.rect.centerx, self.rect.centery, "+205")
                    exp.Exp(self.rect.centerx, self.rect.centery, 0)
                self.rect = self.image.get_rect(center=(-1000, -1000))
                self.timeStartMove = 0
                self.maxTimeStartMove: int = randint(1000, 4555)
                self.Health = 165

            if self.timeShoot >= 19:
                if self.typeStriker >= 1:
                    bullet.SimpleBullet(self.rect.centerx, self.rect.centery, self.bullet_sprite["1"],
                                        (15, 17), 7, enemy_bullet, 19, True)
                else:
                    bullet.SimpleBullet(self.rect.centerx, self.rect.centery, self.bullet_sprite["1"],
                                        (15, 17), -7, enemy_bullet, 19)
                self.timeShoot = 0
            self.timeShoot += 1

            self.image = sprite_ship["2"]

            if self.FrameDamage < 9:
                self.image = pygame.image.load("ASSETS/sprite/enemy/ship/striker/damage.png").convert_alpha()
                self.FrameDamage += 1
            if self.typeStriker >= 1:
                self.image = pygame.transform.flip(self.image, False, True)
            self.image = pygame.transform.scale(self.image, (37, 41))
