from random import randint

import pygame
from group_conf import enemy_group, enemy_bullet
from item import exp, ammo
from particle import AreaParticle
from scene_config import PLAYER, particle_sprite, sprite_bullet
from bullet import bullet
from txtMoveCreate import Txt

sprite = {"1": pygame.image.load("ASSETS/sprite/enemy/shota/1.png").convert_alpha(),
          "2": pygame.image.load("ASSETS/sprite/enemy/shota/2.png").convert_alpha(),
          "3": pygame.image.load("ASSETS/sprite/enemy/shota/damage.png").convert_alpha()}


class Shota(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = sprite["1"]
        self.image = pygame.transform.scale(self.image, (44, 51))
        self.rect = self.image.get_rect(center=(-900, -900))

        self.add(enemy_group)

        self.idEnemy: int = 0

        self.Health: int = randint(95, 105)

        self.timeStartMove: int = 0

        self.maxTimeStartMove = randint(200, 5595)

        self.FrameDamage = 9.5

        self.timeShoot: int = 0

        self.timeDown: int = 0
        self.maxTimeDown: int = randint(95, 225)

        self.speedX: int = randint(-2, 2)
        self.speedY: int = randint(-2, -1)

        self.luck: int = randint(0, 2)

        self.typeShoot: int = randint(0, 1)

        if self.speedX == 0:
            self.speedX = 1

        self.timeChangeMove: int = 0

    def update(self):
        if self.timeStartMove <= self.maxTimeStartMove * 5:
            if self.timeStartMove == self.maxTimeStartMove:
                self.rect = self.image.get_rect(center=(randint(55, 1200), randint(-65, -35)))
            self.timeStartMove += 1
        else:
            if self.Health <= 0:
                for i in range(randint(5, 15)):
                    AreaParticle(self.rect.centerx, self.rect.centery, particle_sprite["1"], (12, 3))
                SCORE_SET = randint(125, 175)
                PLAYER.SCORE += SCORE_SET
                Txt(self.rect.centerx, self.rect.centery, "+"+str(SCORE_SET))
                if self.luck == 1:
                    exp.Exp(self.rect.centerx, self.rect.centery, 0)
                else:
                    ammo.Ammo(self.rect.centerx, self.rect.centery)
                self.rect = self.image.get_rect(center=(-1000, -1000))
                self.timeStartMove = 0
                self.maxTimeStartMove = randint(200, 4495)
                self.Health = randint(115, 125)
            if self.timeDown < self.maxTimeDown:
                if self.FrameDamage < 6:
                    self.image = sprite["3"]
                    self.FrameDamage += 1
                else:
                    self.image = sprite["1"]
                self.rect.y += 3
                self.timeDown += 1
            else:
                if self.timeShoot >= 57:
                    if self.typeShoot == 0:
                        bullet.SimpleBullet(self.rect.centerx, self.rect.centery+10, sprite_bullet["2"],
                                            (17, 17), 5, enemy_bullet, 35)
                    else:
                        bullet.PersonalMoveBullet(self.rect.centerx, self.rect.centery+15, sprite_bullet["1"],
                                                  (21, 21), -5, 6, enemy_bullet)
                        bullet.PersonalMoveBullet(self.rect.centerx, self.rect.centery + 15, sprite_bullet["1"],
                                                  (21, 21), 5, 6, enemy_bullet)
                        bullet.PersonalMoveBullet(self.rect.centerx, self.rect.centery + 15, sprite_bullet["1"],
                                                  (21, 21), 0, 6, enemy_bullet)
                    self.timeShoot = 0
                self.timeShoot += 1

                self.move()
                if self.FrameDamage < 6:
                    self.image = sprite["3"]
                    self.FrameDamage += 1
            self.image = pygame.transform.scale(self.image, (44, 51))

    def move(self):
        if self.speedX > 0 or self.speedX < 0:
            self.image = sprite["2"]
            if self.speedX < 0:
                self.image = pygame.transform.flip(self.image, True, False)
        self.rect.y += self.speedY
        self.rect.x += self.speedX

        if self.rect.x <= 5 or self.rect.x >= 1220:
            self.speedX = -self.speedX
        if self.rect.y <= 10 or self.rect.y >= 700:
            self.speedY = -self.speedY
