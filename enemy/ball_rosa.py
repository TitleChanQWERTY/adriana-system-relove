from random import randint

import pygame
import math
from group_conf import enemy_group, enemy_bullet
from animator import Animator
from bullet import bullet
from particle import AreaParticle
from scene_config import PLAYER, particle_sprite, sprite_bullet
from item import exp
from txtMoveCreate import Txt

ball_sprite = {"1": pygame.image.load("ASSETS/sprite/enemy/ball_rosa/1.png").convert_alpha(),
               "2": pygame.image.load("ASSETS/sprite/enemy/ball_rosa/2.png").convert_alpha()}


class BallRosa(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = ball_sprite["1"]
        self.image = pygame.transform.scale(self.image, (28, 28))
        self.rect = self.image.get_rect(center=(-1000, -1000))

        self.filename = (ball_sprite["1"], ball_sprite["2"])

        self.Animator = Animator(16, self.filename)

        self.spawn_time = pygame.time.get_ticks()

        self.Health: int = 6

        self.idEnemy: int = 0

        self.add(enemy_group)

        self.direct_time_max = randint(485, 650)

        self.timeStartMove: int = 0

        self.maxTimeStartMove: int = randint(25, 400)

        self.timeShoot: int = 0

        self.FrameDamage: int = 4

        self.itemLuck: int = randint(0, 2)

        self.speed = 0.5

    def update(self):
        if self.timeStartMove <= self.maxTimeStartMove * 5:
            if self.timeStartMove == self.maxTimeStartMove:
                self.rect = self.image.get_rect(center=(randint(10, 1205), randint(-50, -35)))
            self.timeStartMove += 1
        else:
            if self.timeShoot >= 87:
                bullet.SimpleBullet(self.rect.centerx, self.rect.centery + 20, sprite_bullet["1"],
                                    (28, 28), 12, enemy_bullet)
                self.timeShoot = 0
            self.timeShoot += 1
            if self.rect.y > 745 or self.Health <= 0:
                if self.Health <= 0:
                    for i in range(randint(10, 15)):
                        AreaParticle(self.rect.centerx, self.rect.centery, particle_sprite["1"], (8, 1))
                    PLAYER.SCORE += 95
                    Txt(self.rect.centerx, self.rect.centery, "+95")
                    if self.itemLuck == 1:
                        exp.Exp(self.rect.centerx, self.rect.centery, 0)
                self.rect = self.image.get_rect(center=(-1050, -1050))
                self.maxTimeStartMove: int = randint(98, 580)
                self.timeStartMove = 0
                self.Health = 6
            self.rect.y += 3
            self.image = self.Animator.update()

            self.move()

            if self.FrameDamage < 9:
                self.image = pygame.image.load("ASSETS/sprite/enemy/ball_rosa/damage.png").convert_alpha()
                self.FrameDamage += 1
            self.image = pygame.transform.scale(self.image, (28, 28))

    def move(self):
        # Take kemgoblin code (dont bit me kem pls)
        change_direction_time = self.direct_time_max
        time = (self.spawn_time + pygame.time.get_ticks()) / change_direction_time * math.pi
        movement_direction_x = round(math.copysign(1, math.sin(time)))

        self.rect.x += round(movement_direction_x)
        self.rect.y += round(self.speed)

        self.rect.x = max(5, min(1190, self.rect.x))


