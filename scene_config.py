import pygame
from player import player
from config_script import WINDOW_SIZE

SCENE: int = 0

PLAYER = player.Player(WINDOW_SIZE[0]//2, WINDOW_SIZE[1]//2, 10)

particle_sprite = {"1": pygame.image.load("ASSETS/sprite/particle/1.png").convert_alpha(),
                   "2": pygame.image.load("ASSETS/sprite/particle/5.png").convert_alpha()}

sprite_bullet = {"1": pygame.image.load("ASSETS/sprite/bullet/enemy/1.png").convert_alpha(),
                 "2": pygame.image.load("ASSETS/sprite/bullet/enemy/4.png").convert_alpha()}

GameLogo = pygame.image.load("ASSETS/sprite/ui/logo.png")

SELECT_MODE: int = 0
