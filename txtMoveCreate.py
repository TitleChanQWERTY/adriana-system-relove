import pygame
import config_script
from group_conf import particle_group


class Txt(pygame.sprite.Sprite):
    def __init__(self, x, y, txt, color=(255, 255, 0), txt_size=22):
        super().__init__()
        font = pygame.font.Font("ASSETS/font/712_Serif.ttf", txt_size)
        self.image = font.render(txt, config_script.AA_TEXT, color)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.timeLive: int = 0

        self.alpha = 290

    def update(self):
        if self.timeLive >= 75:
            self.kill()
        self.rect.y -= 1
        self.alpha -= 3
        self.image.set_alpha(self.alpha)
        self.timeLive += 1
