from random import randint

import pygame

from group_conf import player_bullet, background_group
from bullet import bullet
import config_script

bullet_sprite = {"1": pygame.image.load("ASSETS/sprite/bullet/player/1.png").convert_alpha(),
                 "2": pygame.image.load("ASSETS/sprite/bullet/player/2.png").convert_alpha(),
                 "3": pygame.image.load("ASSETS/sprite/bullet/player/5.png").convert_alpha(),
                 "4": pygame.image.load("ASSETS/sprite/bullet/player/4.png").convert_alpha(),
                 "5": pygame.image.load("ASSETS/sprite/bullet/player/3.png").convert_alpha(),
                 "6": pygame.image.load("ASSETS/sprite/bullet/player/6.png").convert_alpha()}


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y, speed):
        super().__init__()
        self.image = pygame.image.load("ASSETS/sprite/player/1.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (24, 30))
        self.rect = self.image.get_rect(center=(x, y))

        self.HEALTH: int = 125
        self.SCORE: int = 0
        self.EXP: int = 0
        self.DAMAGE: int = 4
        self.BOMB: int = 3
        self.AMMO: int = 550

        self.timeDamage: int = 0
        self.maxTimeDamage: int = 120

        self.speed: int = speed
        self.ForceMove = [0, 0, 0, 0]

        self.timeShoot = [0, 0]
        self.maxTimeShoot: int = 7

        self.forceMoveBack = [0, 0, 0, 0]

        self.txtUI = ("БАЛЛИ: ", "БОМБИ: ", "ЗДОРОВ'Я: ", "ЕКСП: ", "НАБОЇ: ")

        if config_script.LANGUAGE == "EN":
            self.txtUI = ("SCORE: ", "BOMB: ", "HEALTH: ", "EXP: ", "AMMO: ")

        self.font = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 18)

        self.ScoreTXT = self.font.render(self.txtUI[0] + str(self.SCORE), config_script.AA_TEXT, (255, 255, 0))
        self.HealthTXT = self.font.render(self.txtUI[2] + str(self.HEALTH), config_script.AA_TEXT, (255, 0, 0))
        self.ExpTXT = self.font.render(self.txtUI[3] + str(self.EXP), config_script.AA_TEXT, (22, 243, 255))
        self.BombTXT = self.font.render(self.txtUI[1] + str(self.BOMB), config_script.AA_TEXT, (255, 255, 255))
        self.AmmoTXT = self.font.render(self.txtUI[4] + str(self.AMMO), config_script.AA_TEXT, (255, 255, 255))

    def set_exp(self):
        if self.EXP >= 55:
            self.DAMAGE = self.EXP // 90 * 3 + 4
        if self.EXP >= 65:
            self.maxTimeShoot = 5

    def update(self):
        if self.AMMO < 0:
            self.AMMO = 0
        key = pygame.key.get_pressed()
        if key[pygame.K_LSHIFT]:
            pygame.draw.circle(config_script.sc, (255, 125, 255), (self.rect.center[0] + 1, self.rect.center[1] + 1),
                               28, 3)

        if key[pygame.K_k]:
            self.shoot()

        self.inertia_move(key)

        self.ScoreTXT = self.font.render(self.txtUI[0] + str(self.SCORE), config_script.AA_TEXT, (255, 255, 0))
        self.HealthTXT = self.font.render(self.txtUI[2] + str(self.HEALTH), config_script.AA_TEXT, (255, 225, 255))
        self.ExpTXT = self.font.render(self.txtUI[3] + str(self.EXP), config_script.AA_TEXT, (92, 243, 255))
        self.BombTXT = self.font.render(self.txtUI[1] + str(self.BOMB), config_script.AA_TEXT, (255, 255, 255))
        self.AmmoTXT = self.font.render(self.txtUI[4] + str(self.AMMO), config_script.AA_TEXT, (255, 255, 255))

        self.ScoreTXT.set_alpha(self.SCORE - 50)
        self.ExpTXT.set_alpha(self.EXP)

        config_script.sc.blit(self.ScoreTXT, self.ScoreTXT.get_rect(center=(150, 25)))
        config_script.sc.blit(self.HealthTXT, self.HealthTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 30)))
        config_script.sc.blit(self.ExpTXT, self.ExpTXT.get_rect(center=(1150, 25)))
        config_script.sc.blit(self.BombTXT,
                              self.BombTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 - 105, 700)))
        config_script.sc.blit(self.AmmoTXT,
                              self.AmmoTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 + 105, 700)))

    def shoot(self):
        if self.timeShoot[0] >= self.maxTimeShoot:
            bullet.SimpleBullet(self.rect.centerx, self.rect.centery - 10, bullet_sprite["1"],
                                (14, 14), -15, player_bullet)
            if self.AMMO > 0:
                if self.EXP >= 15:
                    bullet.SimpleBullet(self.rect.centerx - 15, self.rect.centery + 8, bullet_sprite["2"],
                                        (7, 17), -19, player_bullet)
                    bullet.SimpleBullet(self.rect.centerx + 15, self.rect.centery + 8, bullet_sprite["2"],
                                        (7, 17), -19, player_bullet)
                    self.AMMO -= 1
                if self.EXP >= 25:
                    bullet.SimpleBullet(self.rect.centerx, self.rect.centery - 10, bullet_sprite["6"],
                                        (4, 12), -10, player_bullet)
                if self.EXP >= 50:
                    bullet.PersonalMoveBullet(self.rect.centerx, self.rect.centery, bullet_sprite["4"],
                                              (18, 18), randint(-15, 15), randint(-15, -10), player_bullet)
                if self.EXP >= 90:
                    bullet.PersonalMoveBullet(self.rect.centerx + 3, self.rect.centery, bullet_sprite["5"],
                                              (17, 17), -2, -15, player_bullet)
                    bullet.PersonalMoveBullet(self.rect.centerx, self.rect.centery, bullet_sprite["5"],
                                              (17, 17), 0, -15, player_bullet)
                    bullet.PersonalMoveBullet(self.rect.centerx - 3, self.rect.centery, bullet_sprite["5"],
                                              (17, 17), 2, -15, player_bullet)
                    self.AMMO -= 1
            self.timeShoot[0] = 0
        self.timeShoot[0] += 1
        if self.EXP >= 130 and self.AMMO > 0:
            if self.timeShoot[1] >= 3:
                bullet.SimpleBullet(self.rect.centerx - 9, self.rect.centery - 10, bullet_sprite["3"],
                                    (3, 10), -15, player_bullet)
                bullet.SimpleBullet(self.rect.centerx + 9, self.rect.centery - 10, bullet_sprite["3"],
                                    (3, 10), -15, player_bullet)
                self.AMMO -= 1
                self.timeShoot[1] = 0
            self.timeShoot[1] += 1

    def inertia_move(self, key):

        if key[pygame.K_w]:
            for back in background_group:
                back.rect.y += self.forceMoveBack[0]
            if self.forceMoveBack[0] < 7:
                self.forceMoveBack[0] += 0.1
            if self.ForceMove[0] < self.speed:
                self.ForceMove[0] += 0.7
            self.rect.y -= self.ForceMove[0]
        else:
            if self.ForceMove[0] > 0:
                self.ForceMove[0] -= 0.4
                self.rect.y -= self.ForceMove[0]
            for back in background_group:
                back.rect.y += self.forceMoveBack[0]
            if self.forceMoveBack[0] > 0:
                self.forceMoveBack[0] -= 0.1

        if key[pygame.K_s]:
            for back in background_group:
                back.rect.y -= self.forceMoveBack[1]
            if self.forceMoveBack[1] < 7:
                self.forceMoveBack[1] += 0.1
            if self.ForceMove[1] < self.speed:
                self.ForceMove[1] += 0.7
            self.rect.y += self.ForceMove[1]
        else:
            if self.ForceMove[1] > 0:
                self.ForceMove[1] -= 0.4
                self.rect.y += self.ForceMove[1]
            for back in background_group:
                back.rect.y -= self.forceMoveBack[1]
            if self.forceMoveBack[1] > 0:
                self.forceMoveBack[1] -= 0.1

        if key[pygame.K_d]:
            for back in background_group:
                back.rect.x -= self.forceMoveBack[2]
            if self.forceMoveBack[2] < 7:
                self.forceMoveBack[2] += 0.1
            if self.ForceMove[2] < self.speed:
                self.ForceMove[2] += 0.7
            self.rect.x += self.ForceMove[2]

        else:
            if self.ForceMove[2] > 0:
                self.ForceMove[2] -= 0.4
                self.rect.x += self.ForceMove[2]
            for back in background_group:
                back.rect.x -= self.forceMoveBack[2]
            if self.forceMoveBack[2] > 0:
                self.forceMoveBack[2] -= 0.1

        if key[pygame.K_a]:
            for back in background_group:
                back.rect.x += self.forceMoveBack[3]
            if self.forceMoveBack[3] < 7:
                self.forceMoveBack[3] += 0.1
            if self.ForceMove[3] < self.speed:
                self.ForceMove[3] += 0.7
            self.rect.x -= self.ForceMove[3]
        else:
            if self.ForceMove[3] > 0:
                self.ForceMove[3] -= 0.4
                self.rect.x -= self.ForceMove[3]
            for back in background_group:
                back.rect.x += self.forceMoveBack[3]
            if self.forceMoveBack[3] > 0:
                self.forceMoveBack[3] -= 0.1

        self.rect.x = max(10, min(config_script.WINDOW_SIZE[0] - 35, self.rect.x))
        self.rect.y = max(10, min(config_script.WINDOW_SIZE[1] - 35, self.rect.y))
