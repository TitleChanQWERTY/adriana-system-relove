import pygame
pygame.init()

LANGUAGE = "UA"

WINDOW_SIZE = (1280, 720)
sc = pygame.display.set_mode(WINDOW_SIZE)

running: bool = True
FPS: int = 60

clock = pygame.time.Clock()

# Option parameters
AA_TEXT: bool = False


SFX_VOLUME: float = 1
MUSIC_VOLUME: float = 1
