import pygame
from config_script import WINDOW_SIZE
from group_conf import effect_group


class Fill(pygame.sprite.Sprite):
    def __init__(self, alpha, alpha_time, alpha_live, color):
        super().__init__()
        self.image = pygame.Surface(WINDOW_SIZE)
        self.alpha = alpha
        self.alpha_live = alpha_live
        self.alpha_time = alpha_time
        self.image.set_alpha(self.alpha)
        self.image.fill(color)
        self.rect = self.image.get_rect()

        self.add(effect_group)

    def update(self):
        self.alpha += self.alpha_time
        self.image.set_alpha(self.alpha)

        if self.alpha_time < 0:
            if self.alpha < self.alpha_live:
                self.kill()
        else:
            if self.alpha > self.alpha_live:
                self.kill()

