import pygame

from player import player
from scene import scene_game
import scene_config
import config_script
import group_conf


def reset():
    scene_config.PLAYER = player.Player(config_script.WINDOW_SIZE[0]//2, config_script.WINDOW_SIZE[1]//2, 10)

    for eff in group_conf.effect_group:
        eff.kill()
    for e in group_conf.enemy_group:
        e.kill()
    for item in group_conf.item_group:
        item.kill()
    for particle in group_conf.particle_group:
        particle.kill()
    for back in group_conf.background_group:
        back.kill()
    for bullet_p in group_conf.player_bullet:
        bullet_p.kill()
    for bomb_playet in group_conf.player_bomb:
        bomb_playet.kill()
    for enemy_bull in group_conf.enemy_bullet:
        enemy_bull.kill()

