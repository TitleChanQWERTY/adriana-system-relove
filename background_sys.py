from random import randint
from group_conf import background_group

import pygame

sprite = {"1": pygame.image.load("ASSETS/sprite/back_particle/1.png").convert_alpha(),
          "2": pygame.image.load("ASSETS/sprite/back_particle/2.png").convert_alpha()}


class Background(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.file = (sprite["1"], sprite["2"])
        self.image = self.file[randint(0, len(self.file) - 1)]
        self.size = randint(2, 8)
        self.image = pygame.transform.scale(self.image, (self.size, self.size))

        self.rect = self.image.get_rect(center=(0, 0))

        self.rect.x = randint(-450, 1950)
        self.rect.y = randint(-450, 1950)

        self.image.set_alpha(randint(45, 97))

        self.add(background_group)

        self.speed = 0.6 + randint(0, 3)

    def update(self):
        self.rect.y += self.speed
        if self.rect.y < -500:
            self.rect.y = randint(1900, 1940)
        if self.rect.y > 1990:
            self.rect.y = randint(-450, -350)
        if self.rect.x < -500:
            self.rect.x = randint(1900, 1940)
        if self.rect.x > 1990:
            self.rect.x = randint(-450, -350)
