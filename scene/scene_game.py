from random import randint

import pygame
import config_script
import scene_config
import group_conf
from collider import colliderBullet, colliderItem
from scene import scene_menu, scene_loading
from bullet import bomb
from reset_date import reset
from screenshot_tool import take_shot


def drawScene():
    group_conf.background_group.draw(config_script.sc)
    config_script.sc.blit(scene_config.PLAYER.image, scene_config.PLAYER.rect)
    group_conf.player_bullet.draw(config_script.sc)
    group_conf.enemy_bullet.draw(config_script.sc)
    group_conf.enemy_group.draw(config_script.sc)
    group_conf.player_bomb.draw(config_script.sc)
    group_conf.item_group.draw(config_script.sc)
    group_conf.particle_group.draw(config_script.sc)
    group_conf.effect_group.draw(config_script.sc)


def updateScene():
    scene_config.PLAYER.update()
    group_conf.background_group.update()
    group_conf.player_bullet.update()
    group_conf.enemy_group.update()
    group_conf.enemy_bullet.update()
    group_conf.particle_group.update()
    group_conf.effect_group.update()
    group_conf.item_group.update()
    group_conf.player_bomb.update()


buttonImg = [pygame.image.load("ASSETS/sprite/ui/button/1.png"), pygame.image.load("ASSETS/sprite/ui/button/1.png"),
             pygame.image.load("ASSETS/sprite/ui/button/1.png")]
for i in range(3):
    buttonImg[i] = pygame.transform.scale(buttonImg[i], (175, 75))
yPosButton = 200

fillScreen = pygame.Surface(config_script.WINDOW_SIZE)
fillScreen.fill((0, 0, 3))
alphaFillScreen: int = 0
fillScreen.set_alpha(0)

screenShake_power = 10
screen_shake = 0

Resume_TXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 20).render("Resume",
                                                                                  config_script.AA_TEXT,
                                                                                  (255, 255, 255))
Restart_TXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 19).render("Restart",
                                                                                   config_script.AA_TEXT,
                                                                                   (255, 255, 255))

Exit_TXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 21).render("Exit",
                                                                                config_script.AA_TEXT,
                                                                                (255, 255, 255))

PauseTxt = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 26).render("WARNING! PAUSE IN SCENE!",
                                                                            config_script.AA_TEXT, (255, 255, 255))

select_pause: int = 0

selectorImg = pygame.image.load("ASSETS/sprite/ui/button/selector.png").convert_alpha()
selectorImg = pygame.transform.scale(selectorImg, (200, 100))

yPosSelector: int = 200


def scene():
    global yPosButton, screen_shake, screenShake_power, alphaFillScreen, Resume_TXT, Restart_TXT, Exit_TXT, \
        PauseTxt, select_pause, yPosSelector, selectorImg

    running = True

    isPause: bool = False

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_shot()
                if e.key == pygame.K_ESCAPE:
                    if not isPause:
                        alphaFillScreen = 0
                        isPause = True
                    else:
                        isPause = False
                if e.key == pygame.K_i and scene_config.PLAYER.BOMB > 0:
                    bomb.Bomb(scene_config.PLAYER.rect.centerx, scene_config.PLAYER.rect.centery-10)
                    scene_config.PLAYER.BOMB -= 1
                if isPause:
                    screen_shake = 9
                    screenShake_power = 5
                    if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                        select_pause += 1
                        selectorImg = pygame.transform.flip(selectorImg, randint(0, 1), randint(0, 1))
                    if e.key == pygame.K_UP or e.key == pygame.K_w:
                        select_pause -= 1
                        selectorImg = pygame.transform.flip(selectorImg, randint(0, 1), randint(0, 1))
                    select_pause %= 3
                    if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                        screen_shake = 11
                        screenShake_power = 13
                        match select_pause:
                            case 0:
                                isPause = False
                            case 1:
                                running = False
                                reset()
                                scene_loading.scene_loading_lvl()
                            case 2:
                                running = False
                                reset()
                                scene_menu.scene()
                                screen_shake = 0
                                screenShake_power = 0

        config_script.sc.fill((18, 0, 28))
        drawScene()
        if not isPause:
            updateScene()
            colliderBullet()
            colliderItem()
        else:
            if select_pause == 0:
                yPosSelector = 200
                buttonImg[0].set_alpha(300)
                buttonImg[1].set_alpha(100)
                buttonImg[2].set_alpha(100)
            if select_pause == 1:
                yPosSelector = 350
                buttonImg[0].set_alpha(100)
                buttonImg[1].set_alpha(300)
                buttonImg[2].set_alpha(100)
            if select_pause == 2:
                yPosSelector = 500
                buttonImg[1].set_alpha(100)
                buttonImg[2].set_alpha(300)
                buttonImg[0].set_alpha(100)
            if alphaFillScreen < 180:
                alphaFillScreen += 10
            fillScreen.set_alpha(alphaFillScreen)
            config_script.sc.blit(fillScreen, fillScreen.get_rect(center=(config_script.WINDOW_SIZE[0] // 2,
                                                                          config_script.WINDOW_SIZE[1] // 2)))
            for i in range(3):
                config_script.sc.blit(buttonImg[i], buttonImg[i].get_rect(center=(config_script.WINDOW_SIZE[0] // 2, yPosButton)))
                yPosButton += 150
                if yPosButton > 500:
                    yPosButton = 200
            if config_script.LANGUAGE == "UA":
                Resume_TXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 20).render("Повернутись",
                                                                                                  config_script.AA_TEXT,
                                                                                                  (255, 255, 255))
                Restart_TXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 19).render("Почати Заново",
                                                                                                   config_script.AA_TEXT,
                                                                                                   (255, 255, 255))
                Exit_TXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 21).render("Вийти",
                                                                                                config_script.AA_TEXT,
                                                                                                (255, 255, 255))
                PauseTxt = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 26).render("УВАГА! ПАЗУА НА СЦЕНІ!",
                                                                                            config_script.AA_TEXT,
                                                                                            (255, 255, 255))
            config_script.sc.blit(Resume_TXT, Resume_TXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 195)))
            config_script.sc.blit(Restart_TXT, Restart_TXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 345)))
            config_script.sc.blit(Exit_TXT, Exit_TXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 495)))
            config_script.sc.blit(PauseTxt, PauseTxt.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 65)))
            GameLogo = pygame.transform.scale(scene_config.GameLogo, (165, 110))
            config_script.sc.blit(GameLogo, GameLogo.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 630)))
            config_script.sc.blit(selectorImg, selectorImg.get_rect(center=(config_script.WINDOW_SIZE[0]//2, yPosSelector)))

        render_offset = [0, 0]
        if screen_shake > 0:
            screen_shake -= 1
            render_offset[0] = randint(0, screenShake_power)
            render_offset[1] = randint(0, screenShake_power)
            if screen_shake <= 15 and screenShake_power > 0:
                screenShake_power -= 1
            config_script.sc.blit(pygame.transform.scale(config_script.sc, config_script.WINDOW_SIZE), render_offset)

        pygame.display.update()
        config_script.clock.tick(config_script.FPS)
