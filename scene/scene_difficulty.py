import pygame
import config_script
import scene_config


def scene():
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            config_script.running = False

    config_script.sc.fill((0, 0, 0))
    pygame.display.update()
    config_script.clock.tick(config_script.FPS)

