from random import randint

import pygame
import scene_config
import config_script
from scene import scene_loading, scene_menu
from screenshot_tool import take_shot

story_image = pygame.image.load("ASSETS/sprite/ui/icon/1.png").convert_alpha()
record_image = pygame.image.load("ASSETS/sprite/ui/icon/2.png").convert_alpha()
music_image = pygame.image.load("ASSETS/sprite/ui/icon/3.png").convert_alpha()

story_image = pygame.transform.scale(story_image, (96, 97))
record_image = pygame.transform.scale(record_image, (96, 97))
music_image = pygame.transform.scale(music_image, (96, 97))

frame1 = pygame.image.load("ASSETS/sprite/ui/button/2.png").convert_alpha()
frame2 = pygame.image.load("ASSETS/sprite/ui/button/2.png").convert_alpha()
frame3 = pygame.image.load("ASSETS/sprite/ui/button/2.png").convert_alpha()

frame1 = pygame.transform.scale(frame1, (198, 201))
frame2 = pygame.transform.scale(frame2, (198, 201))
frame3 = pygame.transform.scale(frame3, (198, 201))

select: int = 0

select_frame = pygame.image.load("ASSETS/sprite/ui/button/selector.png").convert_alpha()
select_frame = pygame.transform.scale(select_frame, (226, 225))
xPosSelect = config_script.WINDOW_SIZE[0] // 2 - 440

txt_set = ("SELECT MODE:", "STORY", "RECORD", "MUSIC")

txtMode = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 26).render(txt_set[0], config_script.AA_TEXT,
                                                                               (255, 255, 255))

txtModeSelect = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 19).render(txt_set[1], config_script.AA_TEXT,
                                                                                 (255, 255, 255))

screen_shake = 11


def scene():
    global txt_set, txtMode, txtModeSelect, screen_shake, select, xPosSelect

    running = True
    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RIGHT:
                    select += 1
                if e.key == pygame.K_LEFT:
                    select -= 1
                select %= 3
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    running = False
                    scene_config.SELECT_MODE = select
                    if select == 0 or select == 1:
                        scene_loading.scene_loading_lvl()
                if e.key == pygame.K_q or e.key == pygame.K_ESCAPE:
                    running = False
                    scene_menu.scene()
                if e.key != pygame.K_F6:
                    screen_shake = 10
                if e.key == pygame.K_F6:
                    take_shot()

        config_script.sc.fill((0, 0, 0))

        if config_script.LANGUAGE == "UA":
            txt_set = ("ОБЕРІТЬ РЕЖИМ:", "ІСТОРІЯ", "РЕКОРД", "МУЗИКАЛЬНИЙ")

        txtMode = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 26).render(txt_set[0], config_script.AA_TEXT,
                                                                                       (255, 255, 255))
        config_script.sc.blit(txtMode, txtMode.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 90)))
        config_script.sc.blit(txtModeSelect, txtModeSelect.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 127)))

        match select:
            case 0:
                xPosSelect = config_script.WINDOW_SIZE[0] // 2 - 441
                story_image.set_alpha(300)
                record_image.set_alpha(90)
                music_image.set_alpha(90)
                frame1.set_alpha(300)
                frame2.set_alpha(100)
                frame3.set_alpha(100)
                txtModeSelect = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 19).render(txt_set[1],
                                                                                                 config_script.AA_TEXT,
                                                                                                 (255, 255, 255))
            case 1:
                xPosSelect = config_script.WINDOW_SIZE[0] // 2 + 10
                story_image.set_alpha(90)
                record_image.set_alpha(300)
                music_image.set_alpha(90)
                frame1.set_alpha(100)
                frame2.set_alpha(300)
                frame3.set_alpha(100)
                txtModeSelect = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 19).render(txt_set[2],
                                                                                                 config_script.AA_TEXT,
                                                                                                 (255, 255, 255))
            case 2:
                xPosSelect = config_script.WINDOW_SIZE[0] // 2 + 460
                story_image.set_alpha(90)
                record_image.set_alpha(90)
                music_image.set_alpha(300)
                frame1.set_alpha(100)
                frame2.set_alpha(100)
                frame3.set_alpha(300)
                txtModeSelect = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 19).render(txt_set[3],
                                                                                                 config_script.AA_TEXT,
                                                                                                 (255, 255, 255))

        config_script.sc.blit(frame1, frame1.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 - 440,
                                                              config_script.WINDOW_SIZE[1] // 2 + 10)))

        config_script.sc.blit(frame2, frame2.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 + 10,
                                                              config_script.WINDOW_SIZE[1] // 2 + 10)))

        config_script.sc.blit(frame3, frame3.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 + 460,
                                                              config_script.WINDOW_SIZE[1] // 2 + 10)))

        config_script.sc.blit(story_image, story_image.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 - 450,
                                                                        config_script.WINDOW_SIZE[1] // 2)))
        config_script.sc.blit(record_image, record_image.get_rect(center=(config_script.WINDOW_SIZE[0] // 2,
                                                                          config_script.WINDOW_SIZE[1] // 2 - 5)))
        config_script.sc.blit(music_image, music_image.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 + 450,
                                                                        config_script.WINDOW_SIZE[1] // 2)))

        config_script.sc.blit(select_frame,
                              select_frame.get_rect(center=(xPosSelect, config_script.WINDOW_SIZE[1] // 2 + 10)))

        render_offset = [0, 0]
        if screen_shake > 0:
            screen_shake -= 1
            render_offset[0] = randint(1, 3)
            render_offset[1] = randint(1, 3)
            config_script.sc.blit(pygame.transform.scale(config_script.sc, config_script.WINDOW_SIZE), render_offset)

        pygame.display.update()
        config_script.clock.tick(config_script.FPS)
