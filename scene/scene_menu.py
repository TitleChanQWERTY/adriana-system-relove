from random import randint

import pygame
import config_script
import scene_config
from particle import AreaParticle
from scene import scene_select_mode
from screenshot_tool import take_shot
from group_conf import particle_group

buttonImg = pygame.image.load("ASSETS/sprite/ui/button/1.png")
buttonImg = pygame.transform.scale(buttonImg, (135, 60))
yPosButton = 250

PlayTXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 22).render("PLAY", config_script.AA_TEXT,
                                                                               (255, 255, 255))

RecordTXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 22).render("INFO", config_script.AA_TEXT,
                                                                                 (255, 255, 255))

OptionTXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 22).render("OPTIONS", config_script.AA_TEXT,
                                                                                 (255, 255, 255))

ExitTXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 22).render("QUIT", config_script.AA_TEXT,
                                                                               (255, 255, 255))

select_menu: int = 0

selectorImg = pygame.image.load("ASSETS/sprite/ui/button/selector.png").convert_alpha()
selectorImg = pygame.transform.scale(selectorImg, (144, 75))

yPosSelector: int = 250

screen_shake = 0

powered_txt = pygame.font.Font("ASSETS/font/712_Serif.ttf", 20).render("Powered by TitleChanQWERTY",
                                                                       config_script.AA_TEXT, (255, 255, 255))


def scene():
    global yPosButton, select_menu, yPosSelector, screen_shake, powered_txt, PlayTXT, RecordTXT, OptionTXT, ExitTXT

    running = True

    timeCrateParticle: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key != pygame.K_F6:
                    screen_shake = 10
                if e.key == pygame.K_F6:
                    take_shot()
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    match select_menu:
                        case 0:
                            running = False
                            scene_select_mode.scene()
                        case 3:
                            pygame.quit()
                if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                    select_menu += 1
                if e.key == pygame.K_UP or e.key == pygame.K_w:
                    select_menu -= 1
                select_menu %= 4

        config_script.sc.fill((0, 0, 0))
        pygame.draw.rect(config_script.sc, (255, 255, 255), (0, yPosSelector, 1290, 3))
        particle_group.draw(config_script.sc)
        particle_group.update()
        for i in range(4):
            config_script.sc.blit(buttonImg, buttonImg.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, yPosButton)))
            yPosButton += 100
            if yPosButton > 550:
                yPosButton = 250
        GameLogo = pygame.transform.scale(scene_config.GameLogo, (220, 140))
        config_script.sc.blit(GameLogo, GameLogo.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 88)))

        if select_menu == 0:
            yPosSelector = 250
        if select_menu == 1:
            yPosSelector = 350
        if select_menu == 2:
            yPosSelector = 450
        if select_menu == 3:
            yPosSelector = 550

        if config_script.LANGUAGE == "UA":
            powered_txt = pygame.font.Font("ASSETS/font/712_Serif.ttf", 20).render("Зробив TitleChanQWERTY",
                                                                                   config_script.AA_TEXT,
                                                                                   (255, 255, 255))

            PlayTXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 22).render("ГРАТИ",
                                                                                           config_script.AA_TEXT,
                                                                                           (255, 255, 255))

            RecordTXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 22).render("ІНФО",
                                                                                             config_script.AA_TEXT,
                                                                                             (255, 255, 255))

            OptionTXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 15).render("НАЛАШТУВАННЯ",
                                                                                             config_script.AA_TEXT,
                                                                                             (255, 255, 255))

            ExitTXT = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 22).render("ВИЙТИ",
                                                                                           config_script.AA_TEXT,
                                                                                           (255, 255, 255))

        config_script.sc.blit(PlayTXT, PlayTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 250)))
        config_script.sc.blit(RecordTXT, RecordTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 350)))
        config_script.sc.blit(OptionTXT, OptionTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 450)))
        config_script.sc.blit(ExitTXT, ExitTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 550)))
        config_script.sc.blit(selectorImg,
                              selectorImg.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, yPosSelector)))

        config_script.sc.blit(powered_txt, powered_txt.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 670)))

        render_offset = [0, 0]
        if screen_shake > 0:
            screen_shake -= 1
            render_offset[0] = randint(1, 3)
            render_offset[1] = randint(1, 3)
            config_script.sc.blit(pygame.transform.scale(config_script.sc, config_script.WINDOW_SIZE), render_offset)

        if timeCrateParticle >= 125:
            for i in range(randint(45, 55)):
                AreaParticle(randint(-45, -25), randint(-15, 700), scene_config.particle_sprite["1"], (8, 1))
                AreaParticle(randint(1285, 1295), randint(-15, 700), scene_config.particle_sprite["1"], (8, 1))
            timeCrateParticle = 0
        timeCrateParticle += 1

        pygame.display.update()
        config_script.clock.tick(config_script.FPS)
