from random import randint

import pygame
import config_script
import scene_config
from scene import scene_game
from background_sys import Background
from enemy import ball_rosa, shota, ship
from item import exp, ammo
from animator import Animator
from group_conf import particle_group


def scene_loading_lvl():
    running = True

    percent = 0

    background_img = ("ASSETS/sprite/ui/loading-img/1.png", "ASSETS/sprite/ui/loading-img/2.png",
                      "ASSETS/sprite/ui/loading-img/3.png")

    select_img = randint(0, len(background_img) - 1)

    back_img = pygame.image.load(background_img[select_img]).convert_alpha()
    back_img = pygame.transform.scale(back_img, (145 * 4, 58 * 4))

    txt_select = ("The system wants to conquer you", "Can love be different?", "We are all the same")

    if config_script.LANGUAGE == "UA":
        txt_select = ("Система хоче підкорити тебе", "Чи видрізняється кохання?", "Ми усі однакові")

    any_txt = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 23).render(
        txt_select[select_img],
        config_script.AA_TEXT,
        (255, 255, 255))

    loading_txt = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 27).render("PRAY...",
                                                                                   config_script.AA_TEXT,
                                                                                   (255, 255, 255))

    loading_anim_sprite = {"1": pygame.image.load("ASSETS/sprite/ui/loading-img/anim/1.png").convert_alpha(),
                           "2": pygame.image.load("ASSETS/sprite/ui/loading-img/anim/2.png").convert_alpha(),
                           "3": pygame.image.load("ASSETS/sprite/ui/loading-img/anim/3.png").convert_alpha(),
                           "4": pygame.image.load("ASSETS/sprite/ui/loading-img/anim/4.png").convert_alpha(),
                           "5": pygame.image.load("ASSETS/sprite/ui/loading-img/anim/5.png").convert_alpha(),
                           "6": pygame.image.load("ASSETS/sprite/ui/loading-img/anim/6.png").convert_alpha()}

    fileAnimLoad = (loading_anim_sprite["1"], loading_anim_sprite["2"], loading_anim_sprite["2"],
                    loading_anim_sprite["3"], loading_anim_sprite["4"], loading_anim_sprite["5"],
                    loading_anim_sprite["6"])

    AnimLoading = Animator(7, fileAnimLoad)

    LoadingImgAnim = loading_anim_sprite["1"]

    for par in particle_group:
        par.kill()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False

        if config_script.LANGUAGE == "UA":
            loading_txt = pygame.font.Font("ASSETS/font/a_OldTyperTitulNr.ttf", 27).render("МОЛИМОСЬ...",
                                                                                           config_script.AA_TEXT,
                                                                                           (255, 255, 255))
            txt_select = ("Система хоче підкорити тебе", "Чи видрізняється кохання?", "Ми усі однакові")

        match percent:
            case 20:
                if scene_config.SELECT_MODE == 1 or scene_config.SELECT_MODE == 2:
                    for i in range(34):
                        ball_rosa.BallRosa()
                    for i in range(65):
                        shota.Shota()
                        ship.Striker()
                        ship.Humiliator()
                    for i in range(405):
                        Background()
                    for i in range(2):
                        exp.Exp(randint(360, 650), randint(125, 350), 0)
                        ammo.Ammo(randint(360, 650), randint(125, 350))
                percent += 1
            case 127:
                if scene_config.SELECT_MODE >= 1:
                    running = False
                    scene_game.scene()
            case _:
                percent += 1

        config_script.sc.fill((0, 0, 0))
        config_script.sc.blit(back_img, back_img.get_rect(center=(config_script.WINDOW_SIZE[0] // 2,
                                                                  config_script.WINDOW_SIZE[1] // 2 - 50)))
        config_script.sc.blit(any_txt, any_txt.get_rect(center=(config_script.WINDOW_SIZE[0] // 2,
                                                                config_script.WINDOW_SIZE[1] // 2 + 100)))
        config_script.sc.blit(loading_txt, loading_txt.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 + 395,
                                                                        config_script.WINDOW_SIZE[1] // 2 + 325)))
        LoadingImgAnim = AnimLoading.update()
        LoadingImgAnim = pygame.transform.scale(LoadingImgAnim, (105, 105))
        config_script.sc.blit(LoadingImgAnim, LoadingImgAnim.get_rect(center=(config_script.WINDOW_SIZE[0] // 2 + 255,
                                                                              config_script.WINDOW_SIZE[1] // 2 + 299)))
        pygame.display.update()
        config_script.clock.tick(config_script.FPS)
