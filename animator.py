class Animator:
    def __init__(self, timeout, filename):
        self.timeout = timeout
        self.filename = filename
        self.Frame: int = 0
        self.time: int = 0

        self.last_item = len(self.filename) - 1

    def update(self):
        if self.time == self.timeout:
            self.Frame += 1
            self.time = 0
        if self.Frame > self.last_item:
            self.Frame = 0
        self.time += 1

        return self.filename[self.Frame]
