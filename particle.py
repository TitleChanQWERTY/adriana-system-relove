from random import randint

import pygame
from group_conf import particle_group


class PhysiclParticle(pygame.sprite.Sprite):
    def __init__(self, x, y, force_falling, force_fly_max, filename, scale):
        super().__init__()
        self.image = filename
        self.image = pygame.transform.scale(self.image, scale)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.force_falling = -force_falling

        self.force_fly = 0
        self.force_fly_max = force_fly_max

    def update(self):
        if self.rect.y >= 725:
            self.kill()

        if self.force_fly_max < 0:
            if self.force_fly > self.force_fly_max:
                self.force_fly -= 1
        else:
            if self.force_fly < self.force_fly_max:
                self.force_fly += 1

        self.rect.y += self.force_falling
        self.rect.x += self.force_fly
        self.force_falling += 0.5


class AreaParticle(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size, alpha=300, is_rotate=True):
        super().__init__()
        self.filename = filename
        self.image = self.filename
        self.size = size
        self.image = pygame.transform.scale(self.image, self.size)

        self.x = x
        self.y = y

        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.speedX = randint(-6, 6)
        self.speedY = randint(-7, 7)

        if self.speedY == 0:
            self.speedY += randint(1, 2)

        self.timeLive: int = 0
        self.maxTimeLive = randint(95, 525)

        self.Angle: int = 0

        self.Alpha = alpha

        self.isRotate = is_rotate

    def update(self):
        if self.rect.y < -95 or self.rect.y >= 930 or self.timeLive >= self.maxTimeLive:
            self.kill()
        self.timeLive += 1

        if self.timeLive >= 42:
            self.Alpha -= 2

        self.y += self.speedY
        self.x += self.speedX

        self.image = self.filename
        self.image = pygame.transform.scale(self.image, self.size)

        if self.isRotate:
            self.Angle += 1 + self.speedY + self.speedX
            self.image = pygame.transform.rotate(self.image, self.Angle+1)

        self.image.set_alpha(self.Alpha)

        self.rect = self.image.get_rect(center=(self.x, self.y))
